<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Users</small></h1>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-4">
            <a href="<?php echo site_url('backend/users/new'); ?>" class="btn btn-info">Create New User</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td><b>User Id</b></td>
                    <td><b>User Name</b></td>
                    <td><b>User Email</b></td>
                    <td><b>User Confirmed</b></td>
                    <td><b>User Date Registration</b></td><!--
                    <td><b>Actions</b></td>-->
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($users)) { ?>
                    <?php foreach($users as $user) { ?>
                        <tr>
                            <td><?php echo $user->user_id; ?></td>
                            <td><?php echo $user->user_name; ?></td>
                            <td><?php echo $user->user_email; ?></td>
                            <td><?php echo $user->user_confirmation_status==1?'Yes':'No'; ?></td>
                            <td><?php echo time_elapsed_string($user->user_date_registration); ?></td>
                            <!--<td>
                                <a href="<?php /*echo site_url('backend/users/edit/'.$user->user_id); */?>">Edit</a>
                            </td>-->
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <ul class="pagination">
                <?php echo !empty($links) ? $links : ''; ?>
            </ul>
        </div>
    </div>

</div><!-- /#page-wrapper -->