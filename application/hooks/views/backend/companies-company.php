<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1><?php echo !empty($company) ? $company->company_name : 'New Company'; ?></h1>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-4">
            <a href="<?php echo site_url('backend/companies'); ?>">Back to companies</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" enctype="multipart/form-data" action="<?php echo !empty($company) ? site_url('backend/companies/save/'.$company->company_id) : site_url('backend/companies/save/'); ?>" method="post" role="form">

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Name</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_name : ''; ?>" name="company_name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Contact</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_contact : ''; ?>" name="company_contact">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Position</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_position : ''; ?>" name="company_position">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Domain</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_domain : ''; ?>" name="company_domain">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Subdomain</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_subdomain : ''; ?>" name="company_subdomain">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Setup Date</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_setup_date : ''; ?>" name="company_setup_date">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Address</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_address : ''; ?>" name="company_address">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Country Id</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_country_id : ''; ?>" name="company_country_id">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company City</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_city : ''; ?>" name="company_city">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Phone</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_phone : ''; ?>" name="company_phone">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Capital Invested</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_capital_invested : ''; ?>" name="company_capital_invested">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Fax</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_fax : ''; ?>" name="company_fax">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Email</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_email : ''; ?>" name="company_email">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Employee</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_employee : ''; ?>" name="company_employee">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Contacts</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_contacts : ''; ?>" name="company_contacts">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Turnover</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($company) ? $company->company_turnover : ''; ?>" name="company_turnover">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div><!-- /#page-wrapper -->