<?php if (!empty($companies)) { ?>
    <?php foreach ($companies as $key => $company) { ?>
        <div class="row listing-row listing <?php echo (!empty($orderedIds) && is_array($orderedIds) && in_array($company->company_id, $orderedIds)) ? 'paid-listing' : ''; ?>" data-id="<?php echo $company->company_id; ?>">
            <div class="col-sm-12">
                <p class="pull-right"><input type="checkbox" checked id="c-<?php echo $key; ?>" name="selected" value="<?php echo $company->company_id; ?>" /><label for="c-<?php echo $key; ?>"></label></p>
                <h3><a href="<?php echo site_url(url_slug($company->company_name).'-'.$company->company_id); ?>"><?php echo $company->company_name; ?></a></h3>
                <p class="muted"><?php echo $company->company_address; ?></p>
                <p>
                    <?php $description  = (!empty($company->company_country_id)) ? '<strong>Country: </strong>'.$countriesDefault[$company->company_country_id]->country_short_name.', ' : ''; ?>
                    <?php $description .= (!empty($company->company_city)) ? '<strong>City: </strong>'.$company->company_city.', ' : ''; ?>
                    <?php $description .= (!empty($company->company_contact)) ? '<strong>Contact: </strong>'.$company->company_contact.', ' : ''; ?>
                    <?php $description .= (!empty($company->company_position)) ? '<strong>Contact`s Position: </strong>'.$company->company_position.', ' : ''; ?>
                    <?php $description .= (!empty($company->company_phone)) ? '<strong>Phone: </strong>'.premium($company->company_phone, false, $orderedIds, $company->company_id).', ' : ''; ?>
                    <?php $description .= (!empty($company->company_fax)) ? '<strong>Fax: </strong>'.premium($company->company_fax, false, $orderedIds, $company->company_id).', ' : ''; ?>
                    <?php $description .= (!empty($company->company_email)) ? '<strong>Email: </strong>'.premium($company->company_email,'@', $orderedIds, $company->company_id).', ' : ''; ?>
                    <?php $description .= (!empty($company->company_website)) ? '<strong>Website: </strong>'.premium($company->company_website, false, $orderedIds, $company->company_id).', ' : ''; ?>
                    <?php $description .= (!empty($company->company_employee)) ? '<strong>Employee: </strong>'.$company->company_employee.', ' : ''; ?>
                    <?php $description .= (!empty($company->company_contacts)) ? '<strong>Other Contacts: </strong>'.$company->company_contacts.', ' : ''; ?>
                    <?php $description .= (!empty($company->company_turnover)) ? '<strong>Turnover: </strong>'.$company->company_turnover.', ' : ''; ?>
                    <?php $description .= (!empty($company->company_capital_invested)) ? '<strong>CUI: </strong>'.$company->company_capital_invested.', ' : ''; ?>
                    <?php echo trim($description, ', '); ?>
                </p>
                <p class="ad-description">
                    <?php // echo !empty($company->company_setup_date) ? '<strong>'.date('Y', strtotime($company->company_setup_date)).'</strong> ' : ''; ?>
                    <?php echo !empty($company->company_domain) ? '<strong>'.$company->company_domain.'</strong> ' : ''; ?>
                    <?php echo !empty($company->company_subdomain) ? '<strong>'.$company->company_subdomain.'</strong> ' : ''; ?>
                </p>
                <p>
                    <span class="classified_links ">
                        <a class="link-info" href="<?php echo site_url('listings/favorite/'.$company->company_id); ?>"><i class="fa fa-star"></i> Add to favorites </a>&nbsp;
                        <!--<a class="link-info" href="<?php /*echo site_url('listings/email/'.$company->company_id); */?>"><i class="fa fa-envelope-o"></i> Send Email</a>&nbsp;-->
                        <?php if (!(!empty($orderedIds) && is_array($orderedIds) && in_array($company->company_id, $orderedIds))) { ?>
                            <a class="link-info submit-form" href="#ci-<?php echo $key; ?>"> <i class="fa fa-shopping-cart"></i> Add to cart </a>&nbsp;
                            <a class="link-info" href="<?php echo site_url('account/available/'.$company->company_id); ?>"> <i class="fa fa-unlock"></i> Free</a>
                        <?php } ?>
                        <form method="post" action="<?php echo site_url('paypal/update'); ?>" id="ci-<?php echo $key; ?>">
                            <input type="hidden" name="type" value="add" />
                            <input type="hidden" name="product_qty" value="1" size="3" />
                            <input type="hidden" name="product_code" value="<?php echo $company->company_id; ?>" />
                            <input type="hidden" name="return_url" value="<?php echo base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>" />
                        </form>
                    </span>
                </p>
            </div>
        </div>
    <?php } ?>
    <?php $page = !empty($page) ? ++$page : 1; ?>
    <input type="hidden" class="page-hidden" value="<?php echo $page ?>" />
<?php } ?>