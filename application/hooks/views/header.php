<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title><?php echo !empty($title) ? $title : 'Companies - from all over the world'; ?> | allworldcompanies.com</title>

    <!-- Bootstrap core CSS -->
    <link id="switch_style" href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/theme.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/dropzone.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/fancybox/helpers/jquery.fancybox-buttons.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/select2/select2.css" media="screen" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<?php if (!empty($notification)) { ?>
    <div class="notification"> 
        <p><?php echo $notification; ?></p>
        <i class="fa fa-times"></i>
    </div>
<?php } ?>

<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="<?php echo base_url(); ?>" class="navbar-brand ">
                <span class="logo"><strong>Companies</strong><span class="handwriting"></span><br />
                <small >from all over the world</small></span>
            </a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right visible-xs">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="<?php echo site_url('login'); ?>">Login</a></li>
                <li><a href="<?php echo site_url('register'); ?>">Register</a></li>
                <li><a href="<?php echo site_url('register'); ?>">Listings</a></li>
                <!--<li><a href="<?php /*echo site_url('account'); */?>">My account</a></li>-->
            </ul>
            <div class="nav navbar-nav navbar-right hidden-xs">
                <div class="row">
                    <div class="pull-right">
                        <?php if (!empty($account)) { ?>
                            <a class="" href="<?php echo site_url('account'); ?>">My profile</a> |
                            <a class="" href="<?php echo site_url('account/searches'); ?>">My searches</a> |
                            <a class="" href="<?php echo site_url('account/favorites'); ?>">Favorites</a> |
                            <a class="" href="<?php echo site_url('account/available'); ?>">Available Companies</a> |
                            <!--<a class="" href="<?php /*echo site_url('account/emails'); */?>">Sent Emails</a> |-->
                            <a class="" href="<?php echo site_url('account/companies'); ?>">Paid Companies</a> |
                            <a class="" href="<?php echo site_url('account/cart'); ?>">Cart <?php echo !empty($sessionProducts) ? '('.count($sessionProducts).') ' : ''; ?></a> |
                            <a class="confirm-js" href="<?php echo site_url('account/logout'); ?>">Logout</a> |
                        <?php } else { ?>
                            <a href="<?php echo site_url('login'); ?>">Login</a> |
                            <a href="<?php echo site_url('register'); ?>">Register</a> |
                        <?php } ?>
                        <a href="<?php echo site_url('listings'); ?>">Listings</a>
                        <!--<a href="<?php /*echo site_url('account'); */?>">My account</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>