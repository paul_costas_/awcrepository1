<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('UPLOAD_STATUS_NOTHING',		0);
define('UPLOAD_STATUS_WAITING',		1);
define('UPLOAD_STATUS_PARSING',		2);
define('UPLOAD_STATUS_FINISHED',	3);

define('AVAILABLE_COMPANIES_TYPE_REGISTRATION',	1);
define('AVAILABLE_COMPANIES_TYPE_PAID',	        2);

define('ORDER_TYPE_SESSION',	    0);
define('ORDER_TYPE_FINISHED',	    1);
define('ORDER_TYPE_FREE',	        2);

define('SEARCH_CATEGORY_COMPANY_NAME',		    'x+b');

define ('PUBLIC_FOLDER', '/home5/qovarioc/public_html/allworldcompanies/original/');

define('PayPalMode',		        'live'); // sandbox or live
define('PayPalApiUsername',		    'dan_api1.allworldcompanies.com'); //PayPal API Username
define('PayPalApiPassword',		    '92DK3WKRSSXW9X39'); //Paypal API password
define('PayPalApiSignature',		'AiPC9BjkCyDFQXbSkoZcgqH3hpacAvL2aAyl0j6GqB2TYP.ASE4cNkTr'); //Paypal API Signature
define('PayPalCurrencyCode',		'USD'); //Paypal Currency Code
define('PayPalReturnURL',		    'http://allworldcompanies.com/original/paypal/process'); //Point to process page
define('PayPalCancelURL',		    'http://allworldcompanies.com/original/paypal/cancel'); //Cancel URL if user clicks cancel

/* End of file constants.php */
/* Location: ./application/config/constants.php */