<?php
class Option extends MY_Model {

    const DB_TABLE = 'new_options';
    const DB_TABLE_PK = 'option_id';

    public $option_id;
    public $option_name;
    public $option_value;
    public $option_date;
}
