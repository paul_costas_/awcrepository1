<?php
Class Model_users extends MY_Model
{
    var $id                     = '';
    var $firstname              = '';
    var $lastname               = '';
    var $email                  = '';
    var $password               = '';
    var $confirmation           = '';
    var $date_confirmation      = '';
    var $date_added             = '';

    function __construct()
    {
        parent::__construct();
    }
}