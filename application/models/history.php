<?php

class History extends MY_Model {

    const DB_TABLE = 'new_history';
    const DB_TABLE_PK = 'history_id';

    public $history_id;
    public $history_date;
}