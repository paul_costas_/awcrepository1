<?php
class Note extends MY_Model {

    const DB_TABLE = 'new_notes';
    const DB_TABLE_PK = 'note_id';

    public $note_id;
    public $note_user_id;
    public $note_subject;
    public $note_content;
    public $note_files;
    public $note_date;
}
