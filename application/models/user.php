<?php

class User extends MY_Model {

    const DB_TABLE = 'new_users';
    const DB_TABLE_PK = 'user_id';

    public $user_id;
    public $user_admin;
    public $user_available_companies;
    public $user_name;
    public $user_email;
    public $user_password;
    public $user_company_name;
    public $user_website;
    public $user_address;
    public $user_mobile;
    public $user_phone;
    public $user_fax;
    public $user_about;
    public $user_avatar;
    public $user_confirmation;
    public $user_confirmation_status;
    public $user_date_confirmation;
    public $user_date_registration;

    public function getByEmailAndPassword($email, $password)
    {
        $return = $this->db->query('SELECT * FROM '.$this::DB_TABLE.' WHERE `user_email` = "'.trim($email).'" AND `user_password` = "'.sha1(trim($password)).'" LIMIT 1');
        return $return->row();
    }

    public function getByEmail($email)
    {
        $return = $this->db->query('SELECT * FROM '.$this::DB_TABLE.' WHERE `user_email` = "'.trim($email).'" LIMIT 1');
        return $return->row();
    }
       
	 function insertData($table,$data)
	{
			
			$id = $this->db->insert($table,$data);
			if($id == 1)
			{
			 return "true";	
			}else{
			return "false";
			}
	}
         

}