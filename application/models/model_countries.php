<?php
Class Model_countries extends MY_Model
{
    var $id                 = '';
    var $iso2               = '';
    var $short_name         = '';
    var $long_name          = '';
    var $iso3               = '';
    var $numcode            = '';
    var $un_member          = '';
    var $calling_code       = '';
    var $cctld              = '';

    function __construct()
    {
        parent::__construct();
    }
}