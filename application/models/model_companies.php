<?php
Class Model_companies extends MY_Model
{
    var $id                     = '';
    var $company_name           = '';
    var $contact                = '';
    var $position               = '';
    var $subdomain0             = '';
    var $subdomain1             = '';
    var $subdomain2             = '';
    var $subdomain3             = '';
    var $subdomain4             = '';
    var $set_up_date            = '';
    var $address                = '';
    var $country                = '';
    var $city                   = '';
    var $district               = '';
    var $index                  = '';
    var $phone                  = '';
    var $area_phone_code        = '';
    var $capital_invested       = '';
    var $fax                    = '';
    var $email                  = '';
    var $rating                 = '';
    var $ratings                = '';
    var $votes                  = '';
    var $contact3               = '';
    var $employee               = '';
    var $turnover               = '';
    var $contact1               = '';
    var $contact2               = '';

    function __construct()
    {
        parent::__construct();
    }
}