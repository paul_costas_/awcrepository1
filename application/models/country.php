<?php

class Country extends MY_Model {

    const DB_TABLE = 'new_countries';
    const DB_TABLE_PK = 'country_id';

    public $country_id;
    public $country_iso2;
    public $country_short_name;
    public $country_long_name;
    public $country_iso3;
    public $country_numcode;
    public $country_un_member;
    public $country_calling_code;
    public $country_cctld;

    public function getCountriesWithCompanies(){
        $inCids = array();
        $cids = $this->db->query('SELECT `company_country_id` FROM `new_companies` GROUP BY `company_country_id`')->result();
        if (!empty($cids)) foreach($cids as $cid) $inCids[] = $cid->company_country_id;
        $inCids = implode(',',$inCids);

        $add = !empty($inCids) ? ' WHERE `country_id` IN ('.trim($inCids, ",").')' : '';
        $return = $this->db->query('SELECT `country_id`, `country_short_name` FROM '.$this::DB_TABLE.$add);
        $countries = $return->result();
        return $countries;
    }
}