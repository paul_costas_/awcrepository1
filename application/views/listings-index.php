<div class="jumbotron home-tron-search well ">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="home-tron-search-inner">
                    <div class="row">
                        <div class="col-sm-8 col-xs-9" style="text-align: center">
                            <div class="row">
                                <div class="col-sm-12 col-sm-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon input-group-addon-text">Find the company</span>
                                        <input type="text" class="form-control col-sm-3 search-input" placeholder="e.g. Company Name" >
                                        <div class="input-group-addon hidden-xs" style="display:none">
                                            <div class="btn-group">
                                                <button class="btn dropdown-toggle search-dropdown-current" data-toggle="dropdown" data-by="company_name">
                                                    Company Name
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-3" style="text-align: center">
                            <div class="row">
                                <div class="col-sm-11 pull-right">
                                    <button class="btn btn-primary search-btn">
                                        <i class="icon-search">
                                        </i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;Search
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container"><br />

    <?php if (!empty($account) && $account->user_admin == 1) { ?>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-hover" id="company-details" data-url="<?php echo site_url('ajax/companyDetails'); ?>">
                    <tbody>
                    <tr><td>Total :  </td><td class="total-companies-picked"><?php echo nice_number($total_companies); ?> companies</td></tr>
                    <?php if (!empty($account) && ($account->user_email == 'paul_costas@yahoo.com' || $account->user_email == 'serjfinciuc@gmail.com')) { ?>
                        <tr><td>Time :  </td><td class="total-time-picked"><?php echo round($total_time, 2); ?> seconds</td></tr>
                    <?php } ?>
                    <tr><td>With email :  </td><td class="loading-td companies-wemail-picked"></td></tr>
                    <tr><td>With website :   </td><td class="loading-td companies-wwebsite-picked"></td></tr>
                    <tr><td>With phone number :  </td><td class="loading-td companies-wphone-picked"></td></tr>
                    </tbody>
                </table
            </div>
        </div>
    <?php } ?>

    <div class="row">
        <div class="col-sm-4  hidden-xs">
            <div class="sidebar ">
                <div class="row ">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Filters<?php echo !empty($filter) ? ', <a style="color:#0061c2" href="'.site_url('listings/save').'">save search</a>' : ''; ?>
                        </div>
                        <div class="panel-body">
                            <form class="form-inline mini" method="post" style="margin-bottom: 0px;">
                                <fieldset>
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Company Name</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="company_name" class="form-control" value="<?php echo !empty($filter['company_name']) ? $filter['company_name'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Country</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <select name="company_country_id[]" class="form-control countries populate select2-offscreen" multiple tabindex="-1">
                                                <option value="0">All</option>
                                                <?php if (!empty($countries)) { ?>
                                                    <?php foreach($countries as $country) { ?>
                                                        <option value="<?php echo $country->country_id; ?>" <?php echo (!empty($filter['company_country_id']) && is_array($filter['company_country_id']) && in_array($country->country_id, $filter['company_country_id'])) ? 'selected' : ''; ?> ><?php echo $country->country_short_name; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--<div class="row filter-row">
                                        <div class="col-sm-6">
                                            <label>City</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="company_city" class="form-control" value="<?php /*echo !empty($filter['company_city']) ? $filter['company_city'] : ''; */?>" />
                                        </div>
                                    </div>-->
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Domains</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <!--<input type="text" name="company_domain" class="form-control" value="<?php /*echo !empty($filter['company_domain']) ? $filter['company_domain'] : ''; */?>" />-->
                                            <!--<select name="company_domain[]" class="form-control domains populate select2-offscreen" multiple tabindex="-1">
                                                <option value="0">All</option>
                                                <?php /*if (!empty($domains)) { */?>
                                                    <?php /*foreach($domains as $domain) { */?>
                                                        <option value="<?php /*echo htmlspecialchars($domain->domain_name); */?>" <?php /*echo (!empty($filter['company_domain']) && in_array($domain->domain_name, $filter['company_domain'])) ? 'selected' : ''; */?> ><?php /*echo $domain->domain_name; */?></option>
                                                    <?php /*} */?>
                                                <?php /*} */?>
                                            </select>-->
                                            <input type="text" id="company_domains" name="company_domain"  value="<?php echo !empty($filter['company_domain']) ? $filter['company_domain'] : ''; ?>"  />
                                        </div>
                                    </div>
                                    <!--<div class="row filter-row">
                                        <div class="col-sm-6">
                                            <label>Subdomain</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="company_subdomain" class="form-control" value="<?php /*echo !empty($filter['company_subdomain']) ? $filter['company_subdomain'] : ''; */?>" />
                                        </div>
                                    </div>-->
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Contact Person</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="company_contact" class="form-control" value="<?php echo !empty($filter['company_contact']) ? $filter['company_contact'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Set up date</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="company_setup_date" class="form-control" value="<?php echo !empty($filter['company_setup_date']) ? $filter['company_setup_date'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Address</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="company_address" class="form-control" value="<?php echo !empty($filter['company_address']) ? $filter['company_address'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Phone</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="company_phone" class="form-control" value="<?php echo !empty($filter['company_phone']) ? $filter['company_phone'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Website</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="company_website" class="form-control" value="<?php echo !empty($filter['company_website']) ? $filter['company_website'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Fax</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="company_fax" class="form-control" value="<?php echo !empty($filter['company_fax']) ? $filter['company_fax'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>Email</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="company_email" class="form-control" value="<?php echo !empty($filter['company_email']) ? $filter['company_email'] : ''; ?>" />
                                        </div>
                                    </div>


                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>With Email</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="checkbox" id="with_email" value="1" name="with_email" <?php echo !empty($filter['with_email']) ? 'checked' : ''; ?> />
                                            <label style="display:block" for="with_email" class="submit-form-onclick"></label>
                                        </div>
                                    </div>

                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>With Website</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="checkbox" id="with_website" value="1" name="with_website" <?php echo !empty($filter['with_website']) ? 'checked' : ''; ?> />
                                            <label style="display:block" for="with_website" class="submit-form-onclick"></label>
                                        </div>
                                    </div>

                                    <div class="row filter-row">
                                        <div class="col-sm-5">
                                            <label>With Phone</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="checkbox" id="with_phone" value="1" name="with_phone" <?php echo !empty($filter['with_phone']) ? 'checked' : ''; ?> />
                                            <label style="display:block" for="with_phone" class="submit-form-onclick"></label>
                                        </div>
                                    </div>


                                    <input type="hidden" name="filter" value="1" />

                                    <div class="row filter-row">
                                        <div class="col-sm-2 pull-right" style="margin-top: 10px;">
                                            <button class="btn btn-primary pull-right" type="submit">
                                                Update results
                                            </button>
                                            <button class="btn reset-form" type="reset" style="position: absolute;left: -210px;padding: 10px 15px;background:#858585">
                                                Reset
                                            </button>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-8 pull-right listings">
            <div class="row listing-row" style="margin-top: -10px;">
                <div class="pull-left">
                    <strong>
                        Today, <?php echo date('l jS \of F'); ?>
                    </strong>
                </div>
            </div>
            <div class="row" style="padding: 20px 0 10px 0;">
                <div class="col-sm-11">
                    <form method="post" action="<?php echo site_url('paypal/update'); ?>">
                        <input type="hidden" name="multiple" value="1" />
                        <input type="hidden" name="type" value="add" />
                        <input type="hidden" name="product_qty" value="1" size="3" />
                        <input type="hidden" name="product_code" value="" class="selectedIds" />
                        <input type="hidden" name="return_url" value="<?php echo base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>" />
                        <!--<button class="btn btn-primary buy-all" type="submit" name="submit" value="all">Buy all <?php /*echo nice_number($total_companies); */?> companies</button>-->
                        <button class="btn btn-primary buy-all" type="submit" name="submit" data-url="<?php echo site_url('ajax/companiesFound'); ?>" id="companies-found" value="all">Buy all companies (<span><i class="fa fa-spin fa-spinner"></i></span>)</button>
                        <button class="btn btn-primary buy-selected" type="submit" name="submit" value="page">Buy selected companies</button>
                    </form>
                </div>
                <div class="col-sm-1" style="padding: 10px 0 0 25px;">
                    <b class="buy-checkboxes-toggle-label" style="position: absolute;left: -70px;top:0">Uncheck All</b>
                    <div style="position:relative;top:-20px">
                        <input type="checkbox" class="buy-checkboxes-toggle" checked id="bct" /><label for="bct"></label>
                    </div>
                </div>
            </div>
            <div id="listings">
                <?php echo !empty($listings) ? $listings : '<p><br/>Sorry, there are no companies for you query.</p>'; ?>
            </div>
            <?php echo !empty($listings) ? '<div class="load-more btn btn-primary" style="display: block;">load more</div>' : ''; ?>
        <div>
            <ul class="pagination pull-right">
                <?php echo !empty($links) ? $links : ''; ?>
            </ul>
        </div>
        </div>
    </div>
</div>