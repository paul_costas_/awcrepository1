<style>table td{word-break: break-word;}</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Your Searches</div>
                <div class="panel-body">
                    <?php if (!empty($sessionProducts) && is_array($sessionProducts)) { ?>
                        <form method="post" action="<?php echo site_url('paypal/process'); ?>">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <td style="width:100px">Product Id</td>
                                    <td>Product Name</td>
                                    <td>Product Link</td>
                                    <td>Actions</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $total = 0; $i = 0; $freeCompanies = $account->user_available_companies; ?>

                                <?php foreach($sessionProducts as $key => $product) { ?>
                                    <?php //$product['price'] = $freeCompanies-- > 0 ? 0.00 : $product['price']; ?>
                                    <tr>
                                        <td style="width: 75px;text-align: center">
                                            <?php echo $product['code']; ?>
                                            <input type="hidden" name="item_name[<?php echo $i; ?>]" value="<?php echo $product['name']; ?>" />
                                            <input type="hidden" name="item_code[<?php echo $i; ?>]" value="<?php echo $product['code']; ?>" />
                                            <input type="hidden" name="item_qty[<?php echo $i; ?>]" value="<?php echo $product['qty']; ?>"  />
                                            <input type="hidden" name="item_price[<?php echo $i++; ?>]" value="<?php echo $product['price']; ?>" />
                                        </td>
                                        <td style="word-break: break-word"><?php echo $product['name']; ?></td>
                                        <td style="width: 110px">
                                            <?php if (strpos($product['name'], 'Package') === false) { ?>
                                                <a style="color:#0061c2" href="<?php echo site_url(url_slug($product['name']).'-'.$product['code']); ?>"><abbr title="<?php echo site_url(url_slug($product['name']).'-'.$product['code']); ?>">Link</abbr></a>
                                            <?php } ?>
                                        </td>
                                        <td><a href="<?php echo site_url('paypal/update/?removep='.$product['code'].'&return_url='.base64_encode(site_url('account/cart'))); ?>" style="color:#0061c2">Delete</a></td>
                                    </tr>
                                    <?php $total+=($product['price']*$product['qty']); ?>
                                <?php } ?>
                                </tbody>
                            </table>
                            <br/>
                            <input type="hidden" name="custom" value="<?php echo $account->user_id; ?>" />
                            <b>Total: </b><?php echo $total.' '.PayPalCurrencyCode; ?>, <a href="<?php echo site_url('account/emptyCart'); ?>" style="color:#0061c2">Empty Cart</a>
                            <button class="btn btn-primary pull-right" type="submit">Checkout</button>
                        </form>
                    <?php } else { ?>
                        <p>Your cart is empty.</p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>