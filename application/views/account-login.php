<hr class="topbar"/>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <br /><br /><br /><br />
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-md-11 col-sm-12">
                            <div class="well">
                                <h2>Sign in</h2>
                                <p>If you have an account with us, please log in.</p>


                                <form role="form"  method="post" action="<?php echo site_url('account/login'); ?>">
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" class="form-control" name="email" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" class="form-control"placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <a href="<?php echo site_url('forget-password'); ?>" style="text-decoration: underline">Forget Password?</a>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" name="remember_me"" checked id="remember-me"> Remember me
                                        <label style="float: left;margin-left: -20px;" for="remember-me"></label>
                                    </div><br />
                                    <button type="submit" class="btn btn-primary">Sign in</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 col-md-11 pull-right">
                            <div class="well">
                                <h2>Register</h2>
                                <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p><br />
                                <a href="<?php echo site_url('register'); ?>" class="btn btn-primary">Create an account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br /><br /><br /><br />
        </div>
    </div>
</div>