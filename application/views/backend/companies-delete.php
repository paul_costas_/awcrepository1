<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Delete Companies</small></h1>
        </div>
    </div><!-- /.row -->
    <script>
        $(function () {
            var rGrp = $("input[name=database]");
            rGrp.click(function () {
                $("#processing_columns").html("Processing...Please Wait...");
                var checkedRadio = rGrp.filter(":checked");
                $.post('<?php echo site_url('backend/companies/getColumns'); ?>', {val: checkedRadio.val()}, function (s) {
                    $('#column_nm').html(s);
                    $("#processing_columns").html("");
                });
            });


            $("#btn_del").click(function () {

                if ($('#crieteria').val() == '') {
                    alert("Please select valid value from Step II");
                    return false;
                } else {

                    var r = confirm("Are you sure, you want to delete records?");
                    if (r == true) {
                        var srv = $("input[name=database]").filter(":checked").val();
                        var clVal = $('#crieteria').val();
                        $("#processing_crieteria").html("Processing...Please Wait...");
                        $.post('<?php echo site_url('backend/companies/deleteMultiple'); ?>', {srv: srv, col: clVal}, function (d) {
                            alert("Records Deleted successfully.");
                            $("#processing_crieteria").html("Processing...Please Wait...");
                        });
                    }
                }

            });
        });

        function getColumnData(clVal) {

            if (clVal != '') {
                var srv = $("input[name=database]").filter(":checked").val();
                $("#processing_crieteria").html("Processing...Please Wait...");
                $.post('<?php echo site_url('backend/companies/getCrieteria'); ?>', {srv: srv, col: clVal}, function (d) {
                    $('#crieteria').html(d);
                    $("#processing_crieteria").html("Processing...Please Wait...");
                });


            }

        }
    </script>
    <div class="row">
        <div class="col-lg-4" style=" width: 36% !important;">
<!--            <a href="<?php echo site_url('backend/companies/new'); ?>" class="btn btn-info">Create New Company</a>
            <a href="<?php echo site_url('backend/companies/getCompanyListToDelete'); ?>" class="btn btn-info" style="float: right;margin-right: 10px;">Delete Companies</a>-->
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">

            <div style="margin-top: 20px;"><fieldset>
                    <legend>Select Database:</legend>
                    <span style="margin-left: 10px;">  <input type="radio" name="database" id="maria" value="2"> MariaDB</span>
                    <span style="margin-left: 10px;"><input type="radio" name="database" id="mysql" value="1"> MySQL</span>
                </fieldset>

                <div style="margin-top: 20px;" id="tblColumn"><fieldset>
                        <legend>Step I</legend>
                        <span style="margin-left: 10px;">  
                            Select Field:  <select name="column_nm" id="column_nm" onchange="getColumnData(this.value)">
                                <option value="">Select</option>                        
                            </select>

                        </span>
                        <span style="margin-left: 10px;" id="processing_columns"></span>
                    </fieldset>
                </div> 

                <div style="margin-top: 20px;" id="cretDv"><fieldset>
                        <legend>Step II</legend>
                        <span style="margin-left: 10px;">  
                            Select  <select name="crieteria" id="crieteria">
                                <option value="">Select</option>                        
                            </select>
                            <span style="margin-left: 10px;" id="processing_crieteria"></span>
                        </span>
                        <input type="button" id="btn_del" name="btn_del" value="Delete">
                    </fieldset>
                </div> 
            </div>
        </div>

    </div><!-- /#page-wrapper -->