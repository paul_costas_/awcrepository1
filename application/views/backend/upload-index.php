<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Upload</small></h1><br/>
            <b>Strictly CSV*</b><br/>
            <b>Format*</b> <span>name | contact | position | domain | setup date | address | country  | city | phone | website | capital invested | fax | email | employee | contacts | turnover</span>
            <br/><br/>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-4">
            <form method="post" action="<?php echo site_url('backend/upload/file'); ?>" enctype="multipart/form-data">
                <label for="file" class="btn btn-info">Upload new File</label>
                <input type="file" multiple name="file[]" style="opacity:0;display:none" id="file" />
            </form>
        </div>
        <a href="<?php echo site_url('import'); ?>"  class="btn btn-info" style="float: right;margin-right: 10px;">Import MySQL</a><a href="<?php echo site_url('import_maria'); ?>"  style="float: right;margin-right: 10px;" class="btn btn-info">Import MariaDB</a>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td><b>Filename</b></td>
                        <td><b>Status</b></td>
                        <td><b>Date Added</b></td>
                        <td><b>Delete</b></td>
                    </tr>
                </thead>
                <tbody>
                <?php if (!empty($files)) { ?>
                    <?php foreach($files as $file) { ?>
                        <tr>
                            <td><?php echo $file->upload_filename; ?></td>
                            <td>
                                <?php
                                    switch($file->upload_status) {
                                        case UPLOAD_STATUS_NOTHING:
                                            echo '<p style="color:#7f8c8d">Nothing, <a href="'.site_url('backend/upload/start/'.$file->upload_id).'">start</a></p>';
                                        break;

                                        case UPLOAD_STATUS_WAITING:
                                            echo '<p style="color:#d35400">Waiting, <a href="'.site_url('backend/upload/cancel/'.$file->upload_id).'">cancel</a></p>';
                                        break;

                                        case UPLOAD_STATUS_PARSING:
                                            echo '<p style="color:#2980b9">Parsing</p>';
                                        break;

                                        case UPLOAD_STATUS_FINISHED:
                                            echo '<p style="color:#27ae60">Finished</p>';
                                        break;
                                    }
                                ?>
                            </td>
                            <td><?php echo time_elapsed_string($file->upload_date_added); ?></td>
                            <td><a href="<?php echo site_url('backend/upload/delete/'.$file->upload_id); ?>"><i class="fa fa-times"></i></a></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>