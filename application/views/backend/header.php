<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Backend | Allworldcompanies</title>

    <!-- JavaScript -->
<script src="<?php echo site_url('assets/backend/js/jquery-1.10.2.js'); ?>"></script>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo site_url('assets/backend/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo site_url('assets/backend/css/redactor.css'); ?>" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?php echo site_url('assets/backend/css/sb-admin.css'); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo site_url('assets/backend/font-awesome/css/font-awesome.min.css'); ?>">
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Allworldcompanies Backend</a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="<?php echo empty($this->uri->segments[2]) ? 'active' : ''; ?>"><a href="<?php echo site_url('backend'); ?>">Dashboard</a></li>
                <li class="<?php echo (isset($this->uri->segments[2]) && $this->uri->segments[2] == 'companies') ? 'active' : ''; ?>"><a href="<?php echo site_url('backend/companies'); ?>">Companies</a></li>
                <li class="<?php echo (isset($this->uri->segments[2]) && $this->uri->segments[2] == 'companies' && $this->uri->segments[2] == 'fields_order') ? 'active' : ''; ?>"><a href="<?php echo site_url('backend/companies/fields_order'); ?>">Company Fields Order</a></li>
                <li class="<?php echo (isset($this->uri->segments[2]) && $this->uri->segments[2] == 'orders') ? 'active' : ''; ?>"><a href="<?php echo site_url('backend/orders'); ?>">Orders</a></li>
                <li class="<?php echo (isset($this->uri->segments[2]) && $this->uri->segments[2] == 'users') ? 'active' : ''; ?>"><a href="<?php echo site_url('backend/users'); ?>">Users</a></li>
                <li class="<?php echo (isset($this->uri->segments[2]) && $this->uri->segments[2] == 'options') ? 'active' : ''; ?>"><a href="<?php echo site_url('backend/options'); ?>">Options</a></li>
                <li class="<?php echo (isset($this->uri->segments[2]) && $this->uri->segments[2] == 'upload') ? 'active' : ''; ?>"><a href="<?php echo site_url('backend/upload'); ?>">Upload</a></li>
                <li class="<?php echo (isset($this->uri->segments[2]) && $this->uri->segments[2] == 'notes') ? 'active' : ''; ?>"><a href="<?php echo site_url('backend/notes'); ?>">Notes</a></li>
            </ul>
        </div>
    </nav>