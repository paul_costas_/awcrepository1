<?php if ( ! defined('BASEPATH')) exit;
class Ajax extends Frontend_Controller {

    public function domains()
    {
        $query = $this->input->get('q');
        $result = $this->db->query('select `domain_name` as `id` from `new_domains` where `domain_name` like "'.$query.'%" order by `domain_name` asc limit 25 ')->result();
        echo json_encode($result);
        exit;
    }

    public function companyDetails()
    {
        $companies = $this->company->filter($this->session->userdata('filter'), null, null, 'details');
        echo json_encode($companies);
        exit;
    }

    public function companiesFound()
    {
        echo nice_number((int)$this->company->filter($this->session->userdata('filter'), null, null, 'count'));
        exit;
    }
}