<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Backend_Controller {

    public function index()
    {
        $this->data['total_companies'] = $this->db->count_all_results('new_companies');

        $this->db->where('user_confirmation_status', 1);
        $this->data['total_users'] = $this->db->count_all_results('new_users');

        $this->db->where('order_type', ORDER_TYPE_FINISHED);
        $this->data['total_orders'] = $this->db->count_all_results('new_orders');

        $this->data['today_income'] = $this->order->getTodayIncome();
        $this->data['today_income'] = $this->data['today_income']->price;
        $this->data['total_income'] = $this->order->getTotalIncome();
        $this->data['total_income'] = $this->data['total_income']->price;

        $this->load->view('backend/header', $this->data);
        $this->load->view('backend/dashboard-index', $this->data);
        $this->load->view('backend/footer');
    }
	
	public function change_position()
   {
	 $data=''; 
	 $social = $this->input->post('social',true);//die();
     $where = array('id'=>'2');
	 if($social==1)
	 {
	  $data=0; 
	 }else
	 {
	 $data=1;	 
	 }
	 //echo $data;die(); 
	 $value = array('value'=>$social);
	 //print_r($where);
	//print_r($value);die();
	  $this->default_info->updatedata('socialnetwork',$where,$value);
     redirect('backend/dashboard');
   }
}