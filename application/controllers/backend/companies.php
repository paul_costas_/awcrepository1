<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies extends Backend_Controller {

    public function index($page = 0)
    {
        $page =  (!isset($page) || $page == 0) ? 1 : $page;
        // ----------> Start Pagination
        $this->load->library("pagination");
        $config                             = array();
        $config["base_url"]                 = site_url('backend/companies');
        $config["total_rows"]               = $this->db->count_all_results('new_companies');
        $config["per_page"]                 = 10;
        $config["uri_segment"]              = 3;
        $config['use_page_numbers']         = TRUE;
        $this->pagination->initialize($config);
        $this->data['companies']            = $this->company->search(array(), ($page-1)*$config["per_page"], $config['per_page']);
        $this->data["links"]                = $this->pagination->create_links();
        // ----------> End Pagination

        $this->load->view('backend/header');
        $this->load->view('backend/companies-index', $this->data);
        $this->load->view('backend/footer');
    }

    public function company($id = null)
    {
        if (!empty($id)) $this->data['company'] = $this->company->load($id);
        $this->load->view('backend/header');
        $this->load->view('backend/companies-company', $this->data);
        $this->load->view('backend/footer');
    }

    public function fields_order()
    {
        if (!empty($_POST)) {
            $finalOrder = array();
            $order = $this->input->post('order');
            foreach ($order as $o) {
                $finalOrder[] = $o;
            }
            $this->db->query('DELETE FROM new_default_info WHERE default_info_name = "company_fields_order"');
            $obj = new Default_info();
            $obj->default_info_name = 'company_fields_order';
            $obj->default_info_array = json_encode($finalOrder);
            $obj->default_info_date_added = date('Y-m-d H:i:s');
            $obj->save();

            redirect('backend/companies/fields_order');
        } else {
            $fieldsDB = $this->db->query('SELECT default_info_array FROM new_default_info WHERE default_info_name = "company_fields_order"')->row();
            if (!empty($fieldsDB)) $fieldsDB = json_decode($fieldsDB->default_info_array);
            if (!empty($fieldsDB)) {
                $this->data['fields'] = $fieldsDB;
            } else {
                $columns = $this->db->query('SHOW COLUMNS FROM new_companies')->result();
                if (!empty($columns)) {
                    $this->data['fields'] = array();
                    foreach ($columns as $column) {
                        $this->data['fields'][] = $column->Field;
                    }
                }
            }

            $this->load->view('backend/header');
            $this->load->view('backend/companies-fields-order', $this->data);
            $this->load->view('backend/footer');
        }
    }

    public function fields_order_reset()
    {
        $this->db->query('DELETE FROM new_default_info WHERE default_info_name = "company_fields_order"');
        redirect('backend/companies/fields_order');
    }

    public function save($id = null)
    {
        $obj = new Company();
        $obj->company_id = $id;
        $obj->company_name = $this->input->post('company_name');
        $obj->company_address = $this->input->post('company_address');
        $obj->company_contact = $this->input->post('company_contact');
        $obj->company_position = $this->input->post('company_position');
        $obj->company_phone = $this->input->post('company_phone');
        $obj->company_email = $this->input->post('company_email');
        $obj->company_fax = $this->input->post('company_fax');
        $obj->company_city = $this->input->post('company_city');
        $obj->company_employee = (int)$this->input->post('company_employee');
        $obj->company_turnover = (float)$this->input->post('company_turnover');
        $obj->company_domain = $this->input->post('company_domain');
        $obj->company_subdomain = $this->input->post('company_subdomain');
        $obj->company_country_id = $this->input->post('company_country_id');
        $obj->company_contacts = $this->input->post('company_contacts');
        $obj->company_capital_invested = $this->input->post('company_capital_invested');
        $obj->company_setup_date = $this->input->post('company_setup_date');
        $obj->save();
        if (empty($id)) $obj->company_date_added = date('Y-m-d H:i:s');
        $obj->save();
        redirect('backend/companies');
    }

    public function delete($id)
    {
        $object = new Company();
        $object->company_id = $id;
        $object->delete();
        redirect('backend/companies');
    }
    
      public function getCompanyListToDelete()
    {
         
        $this->data ='';
         $this->load->view('backend/header');
            $this->load->view('backend/companies-delete', $this->data);
            $this->load->view('backend/footer');
    }
    
      public function getColumns()
    {
       if(isset($_POST['val']) && !empty($_POST['val'])){
          $val = $_POST['val'];
          $str ='<option value="">Select</option>';
          if($val ==1){
              
              $columns = $this->db->query('SHOW COLUMNS FROM new_companies')->result();
                if (!empty($columns)) {
                  //  $this->data['fields'] = array();
                    foreach ($columns as $column) {
                        //$this->data['fields'][] = $column->Field;
                        $str .= '<option value="'.$column->Field.'">'.$column->Field.'</option>';
                    }
                    echo $str;
                    exit;
                }
                
          }elseif($val ==2){
              $mariadbcon = $this->load->database('mariadbcon', TRUE);
              $columns = $mariadbcon->query('SHOW COLUMNS FROM tbl_comp_data')->result();
                if (!empty($columns)) {
                   // $this->data['fields'] = array();
                    foreach ($columns as $column) {
                       // $this->data['fields'][] = $column->Field;
                        $str .= '<option value="'.$column->Field.'">'.$column->Field.'</option>';
                    }
                     echo $str;
                    exit;
                }
                
          }
          
       }
          
    }
    
      public function getCrieteria()
    {
       if(isset($_POST['srv']) && !empty($_POST['srv']) && isset($_POST['col']) && !empty($_POST['col'])){
           $srv = mysql_real_escape_string($_POST['srv']);
           $col =mysql_real_escape_string($_POST['col']);
           
             $str ='<option value="">Select</option>';
          if($srv ==1){
              
              $columns = $this->db->query('SELECT DISTINCT `company_id`,'.$col.' FROM `new_companies`')->result();
                if (!empty($columns)) {
                  //  $this->data['fields'] = array();
                    foreach ($columns as $column) {
                       
                        $str .= '<option value="'.$column->company_id.'">'.$column->Field.'</option>';
                    }
                    echo $str;
                    exit;
                }
                
          }elseif($srv ==2){
              $mariadbcon = $this->load->database('mariadbcon', TRUE);
              $columns = $mariadbcon->query('SELECT DISTINCT `company_id`,'.$col.' FROM  `tbl_comp_data`')->result();
                if (!empty($columns)) {
                   // $this->data['fields'] = array();
                    foreach ($columns as $column) {
                     
                        $str .= '<option value="'.$column->company_id.'">'.$column->Field.'</option>';
                    }
                     echo $str;
                    exit;
                }
                
          }
           
       }
    }
      public function deleteMultiple()
    {
         if(isset($_POST['srv']) && !empty($_POST['srv']) && isset($_POST['col']) && !empty($_POST['col'])){
           $srv = mysql_real_escape_string($_POST['srv']);
           $col =mysql_real_escape_string($_POST['col']);
           
              
          if($srv ==1){
              
              $del = $this->db->query('delete from `new_companies` where company_id='.$col);
                if ($del) {
                  
                    echo '1';
                    exit;
                }
                
          }elseif($srv ==2){
              $mariadbcon = $this->load->database('mariadbcon', TRUE);
              $del = $mariadbcon->query('delete from `tbl_comp_data` where company_id='.$col);
                  if ($del){
                   
                     echo '1';
                    exit;
                }
                
          }
           
       }
    }
    
  //  companies-delete.php
    //deleteMultiple
}