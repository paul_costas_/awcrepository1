<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
class Upload extends Backend_Controller {

    public function index($page = 0)
    {
        $this->data['files'] = $this->file->getFiles();
        $this->load->view('backend/header');
        $this->load->view('backend/upload-index', $this->data);
        $this->load->view('backend/footer');
    }

    public function file()
    {
        if (!empty($_FILES['file']['name'])) {
            foreach ($_FILES['file']['name'] as $key => $one) {
                $file_name      = $one;
                $file_size      = $_FILES['file']['size'][$key];
                $file_tmp       = $_FILES['file']['tmp_name'][$key];
                $file_type      = $_FILES['file']['type'][$key];
                $newFolder      = PUBLIC_FOLDER.'waiting/';

                if(!is_dir($newFolder)) mkdir($newFolder, 0777, true);

                move_uploaded_file($file_tmp,$newFolder.$file_name);

                $obj = new File();
                $obj->upload_filename = $file_name;
                $obj->upload_date_added = date('Y-m-d H:i:s');
                $obj->save();
            }
        }
        redirect('backend/upload');
    }

    public function delete($id)
    {
        $file = $this->file->load($id);
        unlink(PUBLIC_FOLDER.'waiting/'.urlencode($file->upload_filename));
        $obj = new File();
        $obj->upload_id = $id;
        $obj->delete();
        redirect('backend/upload');
    }

    public function start($id = null)
    {
        $obj = new File();
        $obj->upload_id = $id;
        $obj->upload_status = UPLOAD_STATUS_WAITING;
        $obj->save();
        redirect('backend/upload');
    }

    public function cancel($id = null)
    {
        $obj = new File();
        $obj->upload_id = $id;
        $obj->upload_status = 'empty';
        $obj->save();
        redirect('backend/upload');
    }
}