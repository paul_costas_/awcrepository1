<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
        $local_set 		= array(
            'path_upld_file_unchk' 	=> 'file_upl_unchk',
            'path_upld_file_chked' 	=> 'file_upl_chked',
            'con_user' 				=> 'a3008128_test',
            'con_psw' 				=> 'romania123',
            'con_database' 			=> 'a3008128_test',
            'con_host' 				=> 'mysql16.000webhost.com'
        );

        error_reporting(E_ALL);
        ini_set('display_errors', 			TRUE);
        ini_set('display_startup_errors',	TRUE);
        date_default_timezone_set('Europe/London');

        define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br>');

        /** Include PHPExcel */
        require_once APPPATH . '/Classes/PHPExcel.php';
        /** Include PHPExcel_IOFactory */
        require_once APPPATH . '/Classes/PHPExcel/IOFactory.php';

    }

    public function index()
    {
        global $columndb;
        global $tblname;
        $data				 = array();
        $data['dataupload']	 =	'';
        $table_exists		 = False;
        $columndb			 = array();
        $colmn_exists		 = False;
        if (isset($_POST['send_file'])) {

            $tblname 		= $_POST['v_upl_tbname'];
            $_SESSION['tblname'] = $tblname;
            if(!empty($tblname)) {
                $table_exists		= $this->common_model->table_exists($tblname);
                if($table_exists)	{
                    $columndb 			= $this->common_model->get_colummns_db($tblname);
                    if(count($columndb)>0)	{
                        $colmn_exists 	= true;
                    }
                }
            }

            if($colmn_exists){

                $date 			= date_create();
                $uploaddir 		= dirname($_SERVER["SCRIPT_FILENAME"]).'/arch_file/';
				$file_name		= date_format($date, 'YmdHis'). '_' . basename($_FILES['v_upl_file']['name']);
			    $uploadfile 	= $uploaddir.$file_name;
				$config['upload_path'] = $uploaddir;
				$config['allowed_types'] = 'xls|xlsx';
				
				$config['file_name'] = $file_name;
			
				$this->load->library('upload', $config);
                $this->common_model->scrie_log("$uploadfile = ".$uploadfile);
                //print_r($config);die();
				//print_r($_FILES['v_upl_file']);
				
				$path = $_FILES['v_upl_file']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
				
				  
                if ($this->upload->do_upload("v_upl_file") ||($ext == 'csv' and move_uploaded_file($_FILES['v_upl_file']['tmp_name'], $uploadfile))) {
					
                    echo "<div id='uploadcontent'>File is valid, and was successfully uploaded.\n";
                    echo date('H:i:s') , EOL;

                    if (substr($uploadfile, -3) == "xls"){
                        $objReader = PHPExcel_IOFactory::createReader('Excel5');
						
						
						/*try {
	$objPHPExcel = PHPExcel_IOFactory::load($uploadfile);
}   catch(Exception $e) {
	die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}


$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
//print_r($allDataInSheet);
//print_r($arrayCount);die();
$msg = '';
for($i=1;$i<=$arrayCount;$i++){
$company_name = trim($allDataInSheet[$i]["A"]);
$company_contact = trim($allDataInSheet[$i]["B"]);
$company_position = trim($allDataInSheet[$i]["C"]);
$company_domain = trim($allDataInSheet[$i]["D"]);
$company_subdomain = trim($allDataInSheet[$i]["E"]);
$company_setup_date = trim($allDataInSheet[$i]["F"]);
$company_address = trim($allDataInSheet[$i]["G"]);
$company_country_id = trim($allDataInSheet[$i]["H"]);
$company_city = trim($allDataInSheet[$i]["I"]);
$company_phone = trim($allDataInSheet[$i]["J"]);
$company_website = trim($allDataInSheet[$i]["K"]);
$company_capital_invested = trim($allDataInSheet[$i]["L"]);
$company_fax = trim($allDataInSheet[$i]["M"]);
$company_email = trim($allDataInSheet[$i]["N"]);
$company_employee = trim($allDataInSheet[$i]["O"]);
$company_contacts = trim($allDataInSheet[$i]["P"]);
$company_turnover = trim($allDataInSheet[$i]["Q"]);
$company_date_added = trim($allDataInSheet[$i]["R"]);


$query = "SELECT company_name FROM new_companies WHERE company_name = '".$company_name."' and company_contact = '".$company_contact."'";
$sql = mysql_query($query) or die(mysql_error());
$recResult = mysql_fetch_array($sql);
$existName = $recResult["company_name"];
if($existName=="") {
$insertTable= mysql_query("insert into new_companies (company_name,company_contact,company_position,company_domain,company_subdomain,company_setup_date,company_address,company_country_id,company_city,company_phone,company_website,company_capital_invested,company_fax,company_email,company_employee,company_contacts,company_turnover,company_date_added) values('".$company_name."','".$company_contact."','".$company_position."','".$company_domain."','".$company_subdomain."','".$company_setup_date."','".$company_address."','".$company_country_id."','".$company_city."','".$company_phone."','".$company_website."','".$company_capital_invested."','".$company_fax."','".$company_email."','".$company_employee."','".$company_contacts."','".$company_turnover."','".$company_date_added."')") or die(mysql_error());


$msg = 'Record has been added. <div style="Padding:20px 0 0 0;"><a href="">Go Back to tutorial</a></div>';
} else {
$msg = 'Record already exist. <div style="Padding:20px 0 0 0;"><a href="">Go Back to tutorial</a></div>';
}
}
echo "<div style='font: bold 18px arial,verdana;padding: 45px 0 0 500px;'>".$msg."</div>";
 */
			
				
				
				
						
						
						
						
                    }elseif(substr($uploadfile, -4) == "xlsx"){
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
						
					    /* try {
	$objPHPExcel = PHPExcel_IOFactory::load($uploadfile);
}   catch(Exception $e) {
	die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}


$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
//print_r($allDataInSheet);
//print_r($arrayCount);die();
$msg = '';
for($i=1;$i<=$arrayCount;$i++){
$company_name = trim($allDataInSheet[$i]["A"]);
$company_contact = trim($allDataInSheet[$i]["B"]);
$company_position = trim($allDataInSheet[$i]["C"]);
$company_domain = trim($allDataInSheet[$i]["D"]);
$company_subdomain = trim($allDataInSheet[$i]["E"]);
$company_setup_date = trim($allDataInSheet[$i]["F"]);
$company_address = trim($allDataInSheet[$i]["G"]);
$company_country_id = trim($allDataInSheet[$i]["H"]);
$company_city = trim($allDataInSheet[$i]["I"]);
$company_phone = trim($allDataInSheet[$i]["J"]);
$company_website = trim($allDataInSheet[$i]["K"]);
$company_capital_invested = trim($allDataInSheet[$i]["L"]);
$company_fax = trim($allDataInSheet[$i]["M"]);
$company_email = trim($allDataInSheet[$i]["N"]);
$company_employee = trim($allDataInSheet[$i]["O"]);
$company_contacts = trim($allDataInSheet[$i]["P"]);
$company_turnover = trim($allDataInSheet[$i]["Q"]);
$company_date_added = trim($allDataInSheet[$i]["R"]);


$query = "SELECT company_name FROM new_companies WHERE company_name = '".$company_name."' and company_contact = '".$company_contact."'";
$sql = mysql_query($query) or die(mysql_error());
$recResult = mysql_fetch_array($sql);
$existName = $recResult["company_name"];
if($existName=="") {
$insertTable= mysql_query("insert into new_companies (company_name,company_contact,company_position,company_domain,company_subdomain,company_setup_date,company_address,company_country_id,company_city,company_phone,company_website,company_capital_invested,company_fax,company_email,company_employee,company_contacts,company_turnover,company_date_added) values('".$company_name."','".$company_contact."','".$company_position."','".$company_domain."','".$company_subdomain."','".$company_setup_date."','".$company_address."','".$company_country_id."','".$company_city."','".$company_phone."','".$company_website."','".$company_capital_invested."','".$company_fax."','".$company_email."','".$company_employee."','".$company_contacts."','".$company_turnover."','".$company_date_added."')") or die(mysql_error());


$msg = 'Record has been added. <div style="Padding:20px 0 0 0;"><a href="">Go Back to tutorial</a></div>';
} else {
$msg = 'Record already exist. <div style="Padding:20px 0 0 0;"><a href="">Go Back to tutorial</a></div>';
}
}
echo "<div style='font: bold 18px arial,verdana;padding: 45px 0 0 500px;'>".$msg."</div>";
 */
			
				
				
				 	
					
						
						
                    }elseif (substr($uploadfile, -3) == "csv"){
                        $objReader = PHPExcel_IOFactory::createReader('CSV');
						
					/*if(($handle = fopen($uploadfile , "r")) !== FALSE) 
			{
			   while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
				 { 
				    //print_r($data);die();
					$num = count($data);
					$query="INSERT INTO new_companies(company_name,company_contact,company_position,company_domain,company_subdomain,company_setup_date,company_address,company_country_id,company_city,company_phone,company_website,company_capital_invested,company_fax,company_email,company_employee,company_contacts,company_turnover,company_date_added)
									VALUES('".$data[0]."','".$data[1]."','".$data[2]."','".$data[3]."','".$data[4]."','".$data[5]."','".$data[6]."','".$data[7]."','".$data[8]."','".$data[9]."','".$data[10]."','".$data[11]."','".$data[12]."','".$data[13]."','".$data[14]."','".$data[15]."','".$data[16]."','".$data[17]."')";
					mysql_query($query) or die(mysql_error());
				 } 
				fclose($handle);
			}*/
				
					
						
                    }

                    $objPHPExcel 	= $objReader->load("$uploadfile");
                    $_SESSION['uploadfile'] = $uploadfile;

                    $this->common_model->get_table(5, $uploadfile, $columndb, $tblname);
                    echo "<br><br><br><br><br>";
                    echo '<input type="button" name="valid_map" value="Generate Mapping" onClick="generate_map(1)">';
                    echo '<br><br>';
                    echo '<div id="show_mapping" style="layer-background-color:lightyellow;background-color:lightyellow;width:40%; margin-left:50px; visibility:hidden;border:2px solid black;padding:0px">
					</div></div>';
                
				  
			
			
			
				
				
				}else {
					//echo 'dheeraj';die();
					echo $this->upload->display_errors();
                    echo "Possible file upload attack!\n";
                    $data['dataupload']	 =	$this->common_model->prepare_upload();
				}
            }	else 	{
                echo "Table not exists.\n";
                $data['dataupload']	 =	$this->common_model->prepare_upload();
            }
        }	elseif(isset($_POST['do_process'])) {
			
			
            $this->common_model->scrie_log("do_process");
            $tblname 		= $_SESSION['tblname'];
            if(!empty($tblname)) {
                $table_exists		= $this->common_model->table_exists($tblname);
                if($table_exists)	{
                    $columndb	= $this->common_model->get_colummns_db($tblname);
                    $map0		= (isset($_POST['map0'])) ? $_POST['map0']: '';
                    $map1		= (isset($_POST['map1'])) ? $_POST['map1']: '';
                    $this->common_model->read_for_import($map0, $map1);
                    $data['dataupload']	 =	$this->common_model->prepare_upload();
                }	else 	{
                    echo "Table not exists.\n";
                    $data['dataupload']	 =	$this->common_model->prepare_upload();
                }
            }

        }	else	{
            $data['dataupload']	 =	$this->common_model->prepare_upload();
        }

        $this->load->view('header');
        $this->load->view('import', $data);
        $this->load->view('footer');
    }

       


}

/* End of file home.php */
/* Location: ./application/controllers/home.php */