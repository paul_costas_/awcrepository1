<?php if ( ! defined('BASEPATH')) exit;
class Account extends Frontend_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
		
		//echo 'dheeraj';die();
        $this->data['title'] = 'Account';
		//$this->data['account'] = 'a' 
        if (empty($this->data['account'])) {
			
            $this->session->set_userdata('notification', 'You have to be logged in if you want to enter on this page.');
            redirect(site_url('login'));
        }
        $this->load->view('header', $this->data);
        $this->load->view('account-index', $this->data);
        $this->load->view('footer');
    }
    public function login()
    {
		//echo $this->session->userdata('id');//die();
        $this->data['title'] = 'Login';
        if (!empty($_POST))
        {
            $obj['user_email'] = trim($this->input->post('email'));
            $obj['user_password'] = sha1(trim($this->input->post('password')));
			//$obj['user_password'] = trim($this->input->post('password'));
            $user = current($this->user->search($obj));
			//print_r($user);die();
            if(!empty($user)){
                if ($user->user_confirmation_status == 1)
                {
                    $this->session->set_userdata('id', $user->user_id);
                }
                else
                {
                    $this->session->set_userdata('notification', 'Please confirm your account, by clicking on the link from email ('.$user->user_email.').');
                    redirect(site_url('login'));
                }

            } else {
                $this->session->set_userdata('notification', 'Email or password is incorrect.');
                redirect(site_url('login'));
            }
			  $email = $this->input->post('email');  
			  $array = array('user_email'=>$email);
			  $this->session->set_userdata($array);//die();
		     //	echo   $this->session->userdata('user_email');die();
            redirect(base_url());
        }
        else
        {
            $this->load->view('header', $this->data);
            $this->load->view('account-login', $this->data);
            $this->load->view('footer');
        }
    }
    public function register()
    {
        $this->data['title'] = 'Register';
        if (!empty($_POST))
        {
            $obj = new User();
            $obj->user_name = $this->input->post('firstname').' '.$this->input->post('lastname');
            $obj->user_email = $this->input->post('email');
            $obj->user_password = sha1($this->input->post('password'));
            $obj->user_confirmation = sha1($this->input->post('password').time());
            $obj->user_date_registration = date('Y-m-d H:i:s');
            $obj->save();

            $acObj = new Available_companies();
            $acObj->available_companies_user_id = $obj->user_id;
            $acObj->available_companies_number = 5;
            $acObj->available_companies_type = AVAILABLE_COMPANIES_TYPE_REGISTRATION;
            $acObj->available_companies_date_added =date('Y-m-d H:i:s');
            $acObj->save();

            $this->load->library('email');
            $this->email->from('info@allworldcompanies.com', '');
            $this->email->to($obj->user_email);
            $this->email->set_mailtype("html");
            $this->email->subject('Registering Confirmation | allworldcompanies.com');
            $this->email->message('Continue here <a href="'.base_url().'account/confirm/'.$obj->user_confirmation.'/'.$obj->user_id.'">'.base_url().'account/confirm/'.$obj->user_confirmation.'</a>.');
            $this->email->send();
            $this->session->set_userdata('notification', 'Thanks for registering, an email was sent to '.$obj->user_email.'. Please confirm.');
			
			  $email = $this->input->post('email'); 
			  $array = array('user_email'=>$email);
			  $this->session->set_userdata($array);
			  redirect(base_url());
        }
        else
        {
            $this->load->view('header', $this->data);
            $this->load->view('account-register', $this->data);
            $this->load->view('footer');
        }
    }

    public function confirm()
    {
        $confirmation = $this->uri->segment(3);
        $id = $this->uri->segment(4);
        if (!empty($id))
        {
            $user = $this->user->load($id);
            if (!empty($confirmation) && $confirmation == $user->user_confirmation)
            {
                $obj = new User();
                $obj->user_id = $id;
                $obj->user_confirmation_status = 1;
                $obj->user_date_confirmation = date('Y-m-d H:i:s');
                $obj->save();
                $this->session->set_userdata('notification', 'Your account was successfully confirmed. Please login to continue.');
                redirect(site_url('login'));
            }
            else
            {
                $this->session->set_userdata('notification', 'Please check again the url from the email, if you feel that something is wrong don`t hesitate to contact us.');
                redirect(site_url('login'));
            }
        }
        else
        {
            $this->session->set_userdata('notification', 'Please check again the url from the email, if you feel that something is wrong don`t hesitate to contact us.');
            redirect(site_url('login'));
        }
    }

    public function save()
    {
        if (empty($this->data['account'])) {
            $this->session->set_userdata('notification', 'You have to be logged in if you want to enter on this page.');
            redirect(site_url('login'));
        }
        if (!empty($_POST))
        {
            if (!empty($_FILES['file']['name'])) {
                $file_name      = url_slug($_FILES['file']['name']).'.'.date('Y-m-d');
                $file_size      = $_FILES['file']['size'];
                $file_tmp       = $_FILES['file']['tmp_name'];
                $file_type      = $_FILES['file']['type'];
                $newFolder      = PUBLIC_FOLDER.'uploads/';
                if(!is_dir($newFolder)) mkdir($newFolder, 0777, true);
                move_uploaded_file($file_tmp,$newFolder.$file_name);
            }

            $obj = new User();
            $obj->user_id = !empty($this->data['account']) ? $this->data['account']->user_id : null;
            $obj->user_name = $this->input->post('user_name');
            $obj->user_email = $this->input->post('user_email');
            $obj->user_company_name = $this->input->post('user_company_name');
            $obj->user_website = $this->input->post('user_website');
            $obj->user_fax = $this->input->post('user_fax');
            $obj->user_mobile = $this->input->post('user_mobile');
            $obj->user_phone = $this->input->post('user_phone');
            $obj->user_address = $this->input->post('user_address');
            $obj->user_about = $this->input->post('user_about');
            if (!empty($file_name)) $obj->user_avatar = $file_name;
            $obj->save();

            $this->session->set_userdata('notification', 'Data successfully saved.');
            redirect(site_url('account'));
        }
        else
        {
            redirect(site_url('account'));
        }
    }

    public function searches()
    {
        if (empty($this->data['account'])) {
            $this->session->set_userdata('notification', 'You have to be logged in if you want to enter on this page.');
            redirect(site_url('login'));
        }
        $obj['search_user_id'] = !empty($this->data['account']) ? $this->data['account']->user_id : null;
        $this->data['searches'] = $this->search->search($obj, null, null, false);
        $this->load->view('header', $this->data);
        $this->load->view('account-searches', $this->data);
        $this->load->view('footer', $this->data);
    }

    public function favorites()
    {
        if (empty($this->data['account'])) {
            $this->session->set_userdata('notification', 'You have to be logged in if you want to enter on this page.');
            redirect(site_url('login'));
        }
        $obj['favorite_user_id'] = !empty($this->data['account']) ? $this->data['account']->user_id : null;
        $this->data['favorites'] = $this->favorite->search($obj, null, null, false);
        foreach($this->data['favorites'] as $key => $fav)
        {
            $company = $this->company->load($fav->favorite_company_id);
            if (!empty($company)) {
                $this->data['favorites'][$key]->company = $company;
            } else {
                unset($this->data['favorites'][$key]);
            }
        }
        $this->load->view('header', $this->data);
        $this->load->view('account-favorites', $this->data);
        $this->load->view('footer');
    }

    public function companies()
    {
        if (empty($this->data['account'])) {
            $this->session->set_userdata('notification', 'You have to be logged in if you want to enter on this page.');
            redirect(site_url('login'));
        }
        $obj['order_user_id'] = !empty($this->data['account']) ? $this->data['account']->user_id : null;
        $obj['order_type'] = ORDER_TYPE_FINISHED;
        $orders = $this->order->search($obj, null, null, false);
        $this->data['paidCompanies'] = array();

        if (!empty($orders)) {
            foreach($orders as $order) {
                $companyIds = explode(',', $order->order_company_ids);
                foreach ($companyIds as $companyId) {
                    $company = $this->company->load(trim($companyId));
                    if (!empty($company)) {
                        $this->data['paidCompanies'][] = (object) array_merge((array) $company, (array) $order);
                    }
                }
            }
        }

        $this->load->view('header', $this->data);
        $this->load->view('account-companies', $this->data);
        $this->load->view('footer');
    }

    public function cart()
    {
        //print_r($this->data);die();		
        if (empty($this->data['account'])) {
            $this->session->set_userdata('notification', 'You have to be logged in if you want to enter on this page.');
            redirect(site_url('login'));
        }
        $this->load->view('header', $this->data);
        $this->load->view('account-cart', $this->data);
        $this->load->view('footer');
    }

    public function available($case = 'default')
    {
        if (empty($this->data['account'])) {
            $this->session->set_userdata('notification', 'You have to be logged in if you want to enter on this page.');
            redirect(site_url('login'));
        }

        $id = str2int($case);
        $case = !empty($id) ? 'company' : $case;
        switch($case) {
            case 'buy':

                break;

            case 'company':
                if ($this->data['availableCompanies']->available_companies_number > 0) {
                    $currentNumber = --$this->data['availableCompanies']->available_companies_number;
                    $obj = new Available_companies();
                    $obj->available_companies_id = $this->data['availableCompanies']->available_companies_id;
                    $obj->available_companies_number = $currentNumber > 0 ? $currentNumber : 'zero';
                    $obj->save();

                    $orderObj = new Order();
                    $orderObj->order_type = ORDER_TYPE_FREE;
                    $orderObj->order_price_company = 0;
                    $orderObj->order_price_total = 0;
                    $orderObj->order_user_id = $this->data['account']->user_id;
                    $orderObj->order_company_ids = $id;
                    $orderObj->order_email = $this->data['account']->user_email;
                    $orderObj->order_date = date('Y-m-d H:i:s');
                    $orderObj->save();
                }
                redirect('account/available');
                break;

            case 'companies':

                break;

            default:
                $obj['order_user_id'] = $this->data['account']->user_id;
                $obj['order_type'] = ORDER_TYPE_FREE;
                $orders = $this->order->search($obj, null, null, false);
                $this->data['companies'] = array();

                if (!empty($orders)) {
                    foreach($orders as $order) {
                        $companyIds = explode(',', $order->order_company_ids);
                        foreach ($companyIds as $companyId) {
                            $company = $this->company->load(trim($companyId));
                            if (!empty($company)) {
                                $this->data['companies'][] = (object) array_merge((array) $company, (array) $order);
                            }
                        }
                    }
                }

                $this->load->view('header', $this->data);
                $this->load->view('account-available', $this->data);
                $this->load->view('footer');
                break;
        }
    }

    public function emptyCart()
    {
        if (empty($this->data['account'])) {
            $this->session->set_userdata('notification', 'You have to be logged in if you want to enter on this page.');
            redirect(site_url('login'));
        }
        $this->session->unset_userdata('products');
        redirect('account/cart');
    }

    public function forget()
    {
        if (!empty($_POST)) {

            $obj = $this->user->getByEmail($this->input->post('email'));

            if (!empty($obj)) {
                $this->load->library('email');
                $this->email->from('info@allworldcompanies.com', '');
                $this->email->to($obj->user_email);
                $this->email->set_mailtype("html");
                $this->email->subject('Password Recovery | allworldcompanies.com');
                $this->email->message('To recover your password click here <a href="'.site_url('forget-password/'.$obj->user_id.'/'.sha1('recover-'.$obj->user_id)).'">'.site_url('forget-password/'.$obj->user_id.'/'.sha1('recover-'.$obj->user_id)).'</a>.');
                $this->email->send();
                $this->session->set_userdata('notification', 'An email was sent to '.$obj->user_email.'. Please recover your password from your email account.');
                redirect(site_url());
            } else {
                $this->session->set_userdata('notification', 'There is no valid account registered with this email. Please check again.');
                redirect(site_url('forget-password'));
            }
        }

        $this->load->view('header', $this->data);
        $this->load->view('account-forget', $this->data);
        $this->load->view('footer');
    }

    public function recover()
    {
        if (!empty($_POST))
        {
            $obj = new User();
            $obj->user_id = $this->input->post('id');
            $obj->user_password = sha1(trim($this->input->post('password')));
            $obj->save();
            $this->session->set_userdata('notification', 'Your password is recovered. Pleas login to continue.');
            redirect(site_url('login'));
        }
        else
        {
            $id = $this->uri->segment(2);
            $encodedId = $this->uri->segment(3);

            if (!empty($id) && !empty($encodedId) && sha1('recover-'.$id) == $encodedId) {
                $this->data['id'] = $id;
                $this->load->view('header', $this->data);
                $this->load->view('account-recover', $this->data);
                $this->load->view('footer');
            } else {
                $this->session->set_userdata('notification', 'This is not a valid link.');
                redirect(site_url('forget-password'));
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('products');
        $this->session->unset_userdata('notification');
        $this->session->unset_userdata('id');
		$this->session->unset_userdata('user_email');
        redirect(base_url());
    }
}